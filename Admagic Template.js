// ==UserScript==
// @id				Admagic
// @name			Admagic
// @version			0.1
// @description		Automatic ads clicking for admagic
// @author			Sampath Vangari <vbsampath@gmail.com>

// @grant			GM_addStyle
// @grant			GM_getResourceText
// @grant			GM_setValue
// @grant			GM_getValue

// @domain			www.admagic.com

// @include			https://www.admagic.in/*
// @include			http://www.admagic.in/*
// @include			http://admagic.in/*

// @updateURL		https://bitbucket.org/vbsampath/admagic/raw/HEAD/Admagic%20Template.js
// @downloadURL		https://bitbucket.org/vbsampath/admagic/raw/HEAD/Admagic%20Template.js

// third party libraries
// @require			https://gist.githubusercontent.com/arantius/3123124/raw/grant-none-shim.js
// @require			http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js
// @require			https://gist.github.com/raw/2625891/waitForKeyElements.js

// admagic user script dependency files
// @icon			https://bitbucket.org/vbsampath/admagic/raw/HEAD/img/favicon.png
// @require			https://bitbucket.org/vbsampath/admagic/raw/HEAD/js/src/libs/jquery.connections.js
// @require			https://bitbucket.org/vbsampath/admagic/raw/HEAD/js/src/libs/watch.js

// admagic user script files
// @resource		html	https://bitbucket.org/vbsampath/admagic/raw/HEAD/html/Admagic.html
// @resource		style	https://bitbucket.org/vbsampath/admagic/raw/HEAD/css/min/Admagic-min.css
// @require			https://bitbucket.org/vbsampath/admagic/raw/HEAD/js/min/Admagic-min.js
// ==/UserScript==
