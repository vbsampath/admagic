// ==UserScript==
// @id           Admagic
// @name         Admagic
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Automatic clicking for admagic
// @author       Sampath Vangari <vbsampath@gmail.com>
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @grant        GM_setValue
// @grant        GM_getValue
// @icon 	     file://E:\Programs\BitBucket\Git\Repositories\Admagic\img\favicon.png
// @require      https://gist.githubusercontent.com/arantius/3123124/raw/grant-none-shim.js
// @include      https://www.admagic.in/*
// @include      http://www.admagic.in/*
// @include      http://admagic.in/*

// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require 	 file://E:\Programs\BitBucket\Git\Repositories\Admagic\js\src\libs\jquery.connections.js
// @require 	 file://E:\Programs\BitBucket\Git\Repositories\Admagic\js\src\libs\Utils.js
// @require 	 file://E:\Programs\BitBucket\Git\Repositories\Admagic\js\src\Config.js

// @require 	 file://E:\Programs\BitBucket\Git\Repositories\Admagic\js\src\Admagic.js
// @resource html      file://E:\Programs\BitBucket\Git\Repositories\Admagic\html\Admagic.html
// @resource style     file://E:\Programs\BitBucket\Git\Repositories\Admagic\css\Admagic.css
// ==/UserScript==
