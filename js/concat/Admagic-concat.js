/**
 * 
 */

var data = 
{
	urls: 
    {
        cash : { url :  "login/cash_wallet.php", base : true },
		withdraw : { url :  "login/with_pay.php", base : true },
		ads : { url :  "login/my_ads.php", base : true },
		security : { url :  "login/security_check.php", base : true },
		login : { url :  "login.php", base : true },
		logout : { url :  "login/logout.php", base : true },
		index : { url :  "index.php", base : true },
		dashboard : { url :  "login/ibo_login.php", base : true },
		dashboardDesktop : { url :  "login/ibo_login.php?ic=D", base : true },
		ad : { url :  "get_ad_amt.php?cd=", base : false },
    },
    waitElements :
    {
    	login : "#mail_frm",
		userId : ".panel-body.list-group",
		security : "#txt_tran_pwd",
		balance : "font:contains(Cash Wallet Transactions)",
		ads : "font:contains(Daily Ads Log)",
    },
    uiElements :
    {
    	buttons : 
    	{
    		back : ".back-button",
    		balance : ".balance-button",
    		withdraw : ".withdraw-button",
    		process : ".process-button",
    		login : ".login-button",
    		logout : ".logout-button",
    		cancel : ".cancel-button",
    		resume : ".resume-button",
    		method : ".method-button",
    		automate : ".automate-button",
    	},
    	toggle :
    	{
    		click : ".click",
    		xhttp : ".xhttp",
    		automateon : ".automate-on",
    		automateoff : ".automate-off",
    	},
    	inputElements :
    	{
    		securityPassword : "#txt_tran_pwd",
    		securityCheckButton : ':input[value="Check"]',
    		adButtons : ':input[value="VIEW AD"]',
    		loginUsername : 'input[name="txt_un"]',
    		loginPassword : 'input[name="txt_pwd"]',
    	},
    	containers :
    	{
    		progressBar : "#progress-bar-container",
    		progressButtons : ".progress-buttons-container",
    		progress : ".progressbar",
    		account : "#account-container",
    		backButton : ".top-left-container",
    		rightButton : ".top-right-content-buttons",
    		admagic : "#admagicCustomHtml",
    		btree : "#btree-container",
    	},
    	labels :
    	{
    		account : ".accounts-label",
    		userId : ".top-right-content-code",
    		accountId : ".top-right-content-account",
    	}
    },
    tree :
    {
    	totalRows : null,
		greenBorder : "1px solid #34a853",
		blueBorder : "1px solid #4285F4",
		hierarchy : 
		{
			1 : [ 2, 3 ],
			2 : [ 4, 5 ],
			3 : [ 6, 7 ],
			4 : [ 8, 9 ],
			5 : [ 10, 11 ],
			6 : [ 12, 13 ],
			7 : [ 14, 15 ],
			8 : [ 16, 17 ],
			9 : [ 18, 19 ],
			10 : [ 20, 21 ],
			11 : [ 22, 23 ],
			12 : [ 24, 25 ],
			13 : [ 26, 27 ],
			14 : [ 28, 29 ],
			15 : [ 30, 31 ]
		},
		breakPoints : [ 1, 3, 7, 15 ]
    },
    enums :
    {
    	labels : 
    	{
    		automate : "automate",
    		ad : "ad",
    		user : "user",
    		login : "login",
    		tree : "tree"
    	},
    	automation :
    	{
    		steps : 
    		{
    			login : "login", 
				adPage : "adPage", 
				securityCheck : "securityCheck", 
				processingAds : "processingAds", 
				logout : "logout"
    		},
    		status :
    		{
    			active : "active",
    			done : "done",
    			stopped : "stopped"
    		}
    	},
    	ad : 
    	{
    		method :
    		{
    			click : "click",
    			xhttp : "xhttp"
    		}
    	}
    },
    delay :
    {
    	automation : 
    		{
    			min : 900,
    			max : 1500
    		},
    	page :
    		{
    			min : 1000
    		},
    	background : null
    },
    logins :
    {
    	1 :
		{
			username : "ADM5991758",
			password : "pass123",
			securityPassword : "pass123",
			parent : 0,
			status : "Active",
			isChild : false,
			row : 1,
			account : 1
		},
		2 :
		{
			username : "ADM5489878",
			password : "pass123",
			securityPassword : "pass123",
			parent : 1,
			status : "Active",
			isChild : false,
			row : 2,
			account : 2
		},
		3 :
		{
			username : "ADM6215472",
			password : "pass123",
			securityPassword : "pass123",
			parent : 1,
			status : "Active",
			isChild : false,
			row : 2,
			account : 3
		},
		4 :
		{
			username : "ADM4154232",
			password : "pass123",
			securityPassword : "pass123",
			parent : 2,
			status : "Active",
			isChild : false,
			row : 3,
			account : 4
		},
		5 :
		{
			username : "ADM7429751",
			password : "pass123",
			securityPassword : "pass123",
			parent : 2,
			status : "Active",
			isChild : false,
			row : 3,
			account : 5
		},
		6 :
		{
			username : "ADM8634728",
			password : "pass123",
			securityPassword : "pass123",
			parent : 3,
			status : "Active",
			isChild : false,
			row : 3,
			account : 6
		},
		7 :
		{
			username : "ADM1492591",
			password : "pass123",
			securityPassword : "pass123",
			parent : 3,
			status : "Active",
			isChild : false,
			row : 3,
			account : 7
		},
		8 :
		{
			username : "ADM6112881",
			password : "pass123",
			securityPassword : "pass123",
			parent : 4,
			status : "Active",
			isChild : false,
			row : 4,
			account : 8
		},
		9 :
		{
			username : "ADM1925568",
			password : "pass123",
			securityPassword : "pass123",
			parent : 4,
			status : "Active",
			isChild : false,
			row : 4,
			account : 9
		},
		10 :
		{
			username : "ADM7118922",
			password : "pass123",
			securityPassword : "pass123",
			parent : 5,
			status : "Active",
			isChild : false,
			row : 4,
			account : 10
		},
		11 :
		{
			username : "ADM3449437",
			password : "pass123",
			securityPassword : "pass123",
			parent : 5,
			status : "Active",
			isChild : false,
			row : 4,
			account : 11
		},
		12 :
		{
			username : "ADM3793443",
			password : "pass123",
			securityPassword : "pass123",
			parent : 6,
			status : "Active",
			isChild : false,
			row : 4,
			account : 12
		},
		13 :
		{
			username : "ADM9522143",
			password : "pass123",
			securityPassword : "pass123",
			parent : 6,
			status : "Active",
			isChild : false,
			row : 4,
			account : 13
		},
		14 :
		{
			username : "ADM6927984",
			password : "pass123",
			securityPassword : "pass123",
			parent : 7,
			status : "Active",
			isChild : false,
			row : 4,
			account : 14
		},
		15 :
		{
			username : "ADM3486915",
			password : "pass123",
			securityPassword : "pass123",
			parent : 7,
			status : "Active",
			isChild : false,
			row : 4,
			account : 15
		},
	
		16 :
		{
			username : "ADM1716158",
			password : "pass123",
			securityPassword : "pass123",
			parent : 8,
			status : "Active",
			isChild : true,
			row : 5,
			account : 16
		},
		17 :
		{
			username : "ADM8164742",
			password : "pass123",
			securityPassword : "pass123",
			parent : 8,
			status : "Active",
			isChild : true,
			row : 5,
			account : 17
		},
		18 :
		{
			username : "ADM5721562",
			password : "pass123",
			securityPassword : "pass123",
			parent : 9,
			status : "Active",
			isChild : true,
			row : 5,
			account : 18
		},
		19 :
		{
			username : "ADM6135933",
			password : "pass123",
			securityPassword : "pass123",
			parent : 9,
			status : "Active",
			isChild : true,
			row : 5,
			account : 19
		},
		20 :
		{
			username : "ADM3241864",
			password : "pass123",
			securityPassword : "pass123",
			parent : 10,
			status : "Active",
			isChild : true,
			row : 5,
			account : 20
		},
		21 :
		{
			username : "ADM5471355",
			password : "pass123",
			securityPassword : "pass123",
			parent : 10,
			status : "Active",
			isChild : true,
			row : 5,
			account : 21
		},
		22 :
		{
			username : "ADM3897812",
			password : "pass123",
			securityPassword : "pass123",
			parent : 11,
			status : "Active",
			isChild : true,
			row : 5,
			account : 22
		},
		23 :
		{
			username : "ADM2173114",
			password : "pass123",
			securityPassword : "pass123",
			parent : 11,
			status : "Active",
			isChild : true,
			row : 5,
			account : 23
		},
		24 :
		{
			username : "ADM5475648",
			password : "pass123",
			securityPassword : "pass123",
			parent : 12,
			status : "Active",
			isChild : true,
			row : 5,
			account : 24
		},
		25 :
		{
			username : "ADM2111612",
			password : "pass123",
			securityPassword : "pass123",
			parent : 12,
			status : "Active",
			isChild : true,
			row : 5,
			account : 25
		},
		26 :
		{
			username : "ADM9229184",
			password : "pass123",
			securityPassword : "pass123",
			parent : 13,
			status : "Active",
			isChild : true,
			row : 5,
			account : 26
		},
		27 :
		{
			username : "ADM9649193",
			password : "pass123",
			securityPassword : "pass123",
			parent : 13,
			status : "Active",
			isChild : true,
			row : 5,
			account : 27
		},
		28 :
		{
			username : "ADM6166652",
			password : "pass123",
			securityPassword : "pass123",
			parent : 14,
			status : "Active",
			isChild : true,
			row : 5,
			account : 28
		},
		29 : 
		{
			username : "ADM2614953",
			password : "pass123",
			securityPassword : "pass123",
			parent : 14,
			status : "Active",
			isChild : true,
			row : 5,
			account : 29
		},
		30 : 
		{
			username : "ADM7484547",
			password : "pass123",
			securityPassword : "pass123",
			parent : 15,
			status : "Active",
			isChild : true,
			row : 5,
			account : 30
		},
		31 : 
		{
			username : "ADM9712315",
			password : "pass123",
			securityPassword : "pass123",
			parent : 15,
			status : "Active",
			isChild : true,
			row : 5,
			account : 31
		}
    },
};function loadUrl(newLocation)
{
	window.location = newLocation;
	return false;
}

function getBaseURL()
{
	return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}

function reload()
{
	location.reload();
}

function getRandomBetweenMilliseconds(lower, upper)
{
	// var x = Math.random() * (2000-3000+1)+3000);
	var milliseconds = Math.floor(Math.random() * (lower - upper + 1) + upper);
	milliseconds = Math.round(milliseconds / 100) * 100;
	return milliseconds;
}

function isEmpty(str)
{
	return (!str || 0 === str.length);
}

function handle_mousedown(e)
{
	window.my_dragging = {};
	my_dragging.pageX0 = e.pageX;
	my_dragging.pageY0 = e.pageY;
	my_dragging.elem = this;
	my_dragging.offset0 = $(this).offset();
	function handle_dragging(e)
	{
		var left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0),
			top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
		$(my_dragging.elem).offset(
		{
			top : top,
			left : left
		});
	}
	function handle_mouseup(e)
	{
		$('body').off('mousemove', handle_dragging).off('mouseup', handle_mouseup);
	}
	$('body').on('mouseup', handle_mouseup).on('mousemove', handle_dragging);
}

function saveToGM(name, obj)
{
	/*
	 * for(var i in obj) GM_setValue(i+'_store',obj[i]);
	 */
	GM_setValue(name, JSON.stringify(obj));
}

function getFromGM(name)
{
	// obj=eval(GM_getValue(name));
	return JSON.parse(GM_getValue(name));
}//functions

function clickLink(link)
{
	var cancelled = false;

	if (document.createEvent)
	{
		var event = document.createEvent("MouseEvents");
		event.initMouseEvent("click", true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		cancelled = !link.dispatchEvent(event);
	}
	else if (link.fireEvent)
	{
		cancelled = !link.fireEvent("onclick");
	}
}

function getAccountForUser(userId, logins)
{
	var returnValue;
	$.each(logins, function(key, value)
	{
		if (value.username == userId)
		{
			returnValue = key;
			return false;
		}
	});
	return returnValue;
}


// admagic functions

function getWalletBalance()
{
	money.totalCredit = $('table:contains("Balance:"):last')["0"].childNodes[1].firstChild.cells[1].innerHTML.slice(0, -2);
	money.totalDebit = $('table:contains("Balance:"):last')["0"].childNodes[1].firstChild.cells[3].innerHTML.slice(0, -2);
	money.balance = $('table:contains("Balance:"):last')["0"].childNodes[1].firstChild.cells[5].innerHTML.slice(0, -2);
}


function sendDelay()
{
	ad = getFromGM(enums.labels.ad);

	var adListItem = adProcessList[ad.processedAds];
	
	if (typeof (adListItem) === "undefined")
	{
		//ads are complete 
		ad.processing = false;
		saveToGM(enums.labels.ad, ad);
		
		//when automation is working this means an account processing
		//is done and next logical step for it is to logout and move
		//to next account
		if(ad.automate)
		{
			automateLogout();
		}
	}
	else
	{
		var adItem = adProcessList[ad.processedAds].value,
			xhttp = new XMLHttpRequest();

		delay.background = getRandomBetweenMilliseconds(delay.automation.min, delay.automation.max);

		xhttp.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				console.log("Processed ad = " + adItem);

				ad.processedAds++;
				saveToGM(enums.labels.ad, ad);

				setupProgressBar();

				timeoutId = setTimeout(sendDelay, delay.background, adItem);
			}
		};
		xhttp.open("GET", urls.ad + adItem, true);
		xhttp.send();
	}
}


function eventHandlers()
{
	// handler for accounts selection
	$(".btree-item").on("click", btreeItemClickEventHandler);

	// handler for account further actions
	$(uiElements.buttons.back).on("click", backButtonClickEventHandler);
	$(uiElements.buttons.balance).on("click", balanceButtonClickEventHandler);
	$(uiElements.buttons.withdraw).on("click", withdrawButtonClickEventHandler);
	$(uiElements.buttons.process).on("click", processButtonClickEventHandler);
	$(uiElements.buttons.login).on("click", loginButtonClickEventHandler);
	$(uiElements.buttons.logout).on("click", logoutButtonClickEventHandler);
	$(uiElements.buttons.cancel).on("click", cancelButtonClickEventHandler);
	$(uiElements.buttons.resume).on("click", resumeButtonClickEventHandler);

	// handler for all account actions
	$(uiElements.buttons.method).on("click", methodButtonClickEventHandler);
	$(uiElements.buttons.automate).on("click", automateButtonClickEventHandler);
}

function resetProgressBar()
{
	// UI customization
	$(uiElements.containers.progressBar).show();
	$(uiElements.containers.progress).hide();
	$(uiElements.containers.progressButtons).hide();
}

function setupBtree()
{
	var elementWidth = 20,
		elementHeight = 20,
		elementMargin = 5,
		spacingsNew = {},
		lastRowElementSpacing = 6;
	
	
	var loginsCount = Object.keys(logins).length;

	switch(true) 
	{
	   case loginsCount == 1 :
	      tree.totalRows = 1;
	      break;
	   case loginsCount == 3: 
		   tree.totalRows = 2;
	      break;
	   case loginsCount == 7:
		   tree.totalRows = 3;
	      break;
	   case loginsCount == 15:
		   tree.totalRows = 4;
		  break;
	   case loginsCount == 31:
		   tree.totalRows = 5;
		  break;
	   case loginsCount > 31 && loginsCount <= 63:
		   tree.totalRows = 6;
		  break;
	}

	saveToGM(enums.labels.tree, tree);
	
	
	// calculate elements in each row
	for (var i = 0; i < tree.totalRows; i++)
	{
		elementsInRows[i] = Math.pow(2, i);
	}

	// calculate the sum of elements till that row
	for (var i = 1; i <= tree.totalRows; i++)
	{
		var sum = 0;
		for (var j = 0; j < i; j++)
		{
			sum = sum + elementsInRows[j];
		}
		elementsTillRow[i - 1] = sum;
	}
	
	
	//based on last row elements calculate maximum 
	var lastRowElementsCount = elementsInRows[tree.totalRows-1],
		lastRowSpacingWidth = 0,
		lastRowTotalElementsWidth = 0,
		lastRowMaximumWidth = 0;
	
	for(var i=lastRowElementsCount-1;i>0;i--)
	{
		if(i!=0)
		{
			lastRowSpacingWidth = lastRowSpacingWidth + lastRowElementSpacing;
		}
	}
	
	lastRowTotalElementsWidth = lastRowElementsCount * elementWidth;
	lastRowMaximumWidth = lastRowTotalElementsWidth + lastRowSpacingWidth;
	
	for(var i=tree.totalRows;i>0;i--)
	{
		
		//get number of elements in this row
		var elementsInThisRow = elementsInRows[i-1],
			spacing = [];
		//add spacings for last row seperately because it doesnt require
		//any formula
		if(i==5)
		{
			//console.log(i);
			for(var k=0;k<=elementsInThisRow;k++)
			{
				//since we are not adding any spacing on left
				//for first element, so we start with second element
				if(k!=0)
				{
					//other elements
					spacing[k-1] = lastRowElementSpacing * (k-1);
				}
			}
			//console.log(spacing);
			
			spacingsNew[i] = spacing;
		}
		else
		{
			//all remaining rows spacings are calculated here
			
			var eachElementSegment = lastRowMaximumWidth/elementsInThisRow,
				segmentEmptySpace = eachElementSegment - elementWidth,
				singleSegmentEmptySpace = segmentEmptySpace/2;
			
			//with element size of 24 and spacing of 8, 5th row of 16 elements
			//4th row = 63
			//3rd row = 126
			//2nd row = 252
			//1st row = 504
			
			
			for(var j=1;j<=elementsInThisRow;j++)
			{
				if(j==1)
				{
					//first element 
					spacing[j-1] = singleSegmentEmptySpace;
				}
				else
				{
					//other elements
					var nextSpacing = (((j-1)*2)+1) * singleSegmentEmptySpace; 
					spacing[j-1] = nextSpacing;
				}
			}
			
			//console.log(spacing);
			//return;
			spacingsNew[i] = spacing;
			
		}
		
	}
	
	//console.log(spacingsNew);
	
	
	var rowElementObjects = {},
		rowObjects = [];

	for (var i = 0; i < tree.totalRows; i++)
	{
		var elementsStart = 0,
			elementsEnd = elementsTillRow[i],
			outerDivElement = [];
		
		if (i != 0)
		{
			elementsStart = elementsTillRow[i - 1];
		}

		for (var j = (elementsStart + 1); j <= elementsEnd; j++)
		{
			var innerDiv = $('<div>'),
				outerDiv = $('<div>'),
				span = $('<span>');
			var spanElement = span.addClass("btree-item-content").attr("id", "a" + j).html(j);
			var innerDivElement = innerDiv.addClass("btree-item").addClass("a" + j).html(spanElement);
			outerDivElement.push(outerDiv.addClass("btree-item-container").html(innerDivElement));
		}

		rowElementObjects[i] = outerDivElement;
	}

	for (var i = 1; i <= tree.totalRows; i++)
	{
		var rowDiv = $('<div>');
		rowDiv.addClass("btree-row-container").addClass("row" + i);
		rowDiv.append(rowElementObjects[i - 1]);
		rowObjects.push(rowDiv);
	}

	// create final html to append to btree main div
	var btree = $(uiElements.containers.btree);
	btree.append(rowObjects);
	
	
	// change css after building html
	
	//building css for account elements left positioning
	// console.log(spacings);
	var color = tree.greenBorder;
	
	
	for (var i = 1; i <= tree.totalRows; i++)
	{
		var elementsInThisRow = elementsInRows[i - 1],
			elementsTillThisRow = elementsInRows[i - 1];
		//console.log("elementsInThisRow = "+elementsInThisRow);
		
		
		for (var j = 0; j < elementsInThisRow; j++, elementsTillThisRow++)
		{
			//console.log("spacing = " + spacingsNew[i][j]);
			
			if (i == tree.totalRows) color = tree.blueBorder;
			//if (i == tree.totalRows && j == 1) continue; 
				
			$('.a' + elementsTillThisRow).css(
			{
				"left" : spacingsNew[i][j],
				"border" : color
			});
		}
	}
	
	//building css for rows for top positioning
	
	//getting top element and btree starting element to calculate height
	var topOffset = $('.btree-container').offset().top - $('.custom-content').offset().top;
	topOffset = 5 * Math.round(topOffset/5);

	//console.log(topOffset);

	
	for (var i = 1; i <= tree.totalRows; i++)
	{
		var top = topOffset + (elementHeight+(2*elementMargin))*(i-1);
		
		$('.row' + i).css(
		{
			"top" : top
		});
		
	}
}


function setupProgressBar()
{

	if (ad.width <= 100)
	{
		if (ad.method == enums.ad.method.click)
		{
			if (ad.processWidth)
			{
				ad.width = ad.width + 100 / ad.totalAds;
			}
		}
		else if (ad.method == enums.ad.method.xhttp)
		{
			ad.width = ad.width + 100 / ad.totalAds;
		}
	}

	// storing updated width because it doesnt get width from html
	// also its not persistent in code after background function calls
	saveToGM(enums.labels.ad, ad);

	// show progressbar now because process starts now
	$(uiElements.containers.progressButtons).show();
	$(uiElements.containers.progressBar).show();
	$(uiElements.containers.progress).show();

	$(uiElements.containers.progress).css(
	{
		"width" : ad.width + '%'
	});
	$(uiElements.containers.progress).text("" + ad.processedAds + "/" + ad.totalAds);
	
	//hide resume and cancel buttons when processing is done
	if(ad.processedAds == ad.totalAds)
	{
		$(uiElements.buttons.cancel).hide();
		$(uiElements.buttons.resume).hide();
	}

}

function uiFirstRun()
{
	// hide account details and back button at start because we have yet to decide
	// an account to login
	$(uiElements.containers.account).hide();
	$(uiElements.containers.backButton).hide();

	// also hide progress bar because we haven't processed ads
	$(uiElements.containers.progressBar).hide();

	//show or hide method button based on set value
	if (ad.method == enums.ad.method.xhttp)
	{
		$(uiElements.toggle.xhttp).show();
		$(uiElements.toggle.click).hide();
	}
	else if (ad.method == enums.ad.method.click)
	{
		$(uiElements.toggle.click).show();
		$(uiElements.toggle.xhttp).hide();
	}
}

function urlBasedActions()
{
	switch (window.location.href)
	{
		//in login and index page reset user object 
		case urls.login: case urls.index:
			
			// reset user object because no one is logged in
			user.userId = "";
			user.accountId = "";
			user.isLoggedIn = false;
			saveToGM(enums.labels.user, user);
			
			break;
		
		//since its in dashboard some user is logged in
		case urls.dashboard: case urls.dashboardDesktop:
			
			//set logged in for visting dashboard for the first time
			if(!user.isLoggedIn)
			{
				// set user as logged in because we are in dashboard
				user.isLoggedIn = true;
				saveToGM(enums.labels.user, user);
			}
			
			break;
			
			//since its in dashboard some user is logged in
		case urls.security:
			var securityPassword = "";
			if(ad.automate)
			{
				var selfAutomate = getFromGM(enums.labels.automate);
				securityPassword = selfAutomate.processingLogin.securityPassword;
			}
			else
			{
				securityPassword = logins[user.accountId].securityPassword;
			}
			
			$(uiElements.inputElements.securityPassword).val(securityPassword);
			var checkButton = $(uiElements.inputElements.securityCheckButton);
			checkButton.click();
			
			break;
	}
}

function attachHTML()
{
	
	// adding custom html for account management and its styleSheets
	var admagicCustomHTML = GM_getResourceText("html"),
		admagicCustomCSS = GM_getResourceText("style");
	GM_addStyle(admagicCustomCSS);
	$("body").append(admagicCustomHTML);
	
	// making our custom div dragabble
	$(uiElements.containers.admagic).mousedown(handle_mousedown);

}


function setupDataObjects()
{
	
	if(getFromGM(enums.labels.login) == null)
	{
		//save and empty object of login to work on when the application data
		//i.e., gm save data is cleared when deleted manually or new browser
		saveToGM(enums.labels.login, login);
	}
	else
	{
		login = getFromGM(enums.labels.login);
	}
	
	if(getFromGM(enums.labels.user) == null)
	{
		//save and empty object of login to work on when the application data
		//i.e., gm save data is cleared when deleted manually or new browser
		saveToGM(enums.labels.user, user);
	}
	else
	{
		user = getFromGM(enums.labels.user);
	}
	
	if(getFromGM(enums.labels.ad) == null)
	{
		//save and empty object of login to work on when the application data
		//i.e., gm save data is cleared when deleted manually or new browser
		saveToGM(enums.labels.ad, ad);
	}
	else
	{
		ad = getFromGM(enums.labels.ad);
	}
}


function getConfigData()
{
	tree = data.tree;
	waitElements = data.waitElements;
	logins = data.logins;
	automatedStepsEnum = data.automateSteps;

	//setting enums
	enums = data.enums;
	
	//setting ui elements
	uiElements = data.uiElements;
	
	//setting delay object
	delay = data.delay;
	delay.background = getRandomBetweenMilliseconds(delay.automation.min, delay.automation.max);
	
	//setting urls
	$.each(data.urls, function(key, value)
	{
		if (value.base)
		{
			urls[key] = getBaseURL() + value.url;
		}
		else
		{
			urls[key] = value.url;
		}
	});
}





function postLoginAccountOperations()
{
	// based on user login change accountID status and buttons in next steps
	if (!isEmpty(user.userId))
	{
		// get 1,2,3 etc based on the ADM123 like code
		user.accountId = getAccountForUser(user.userId, logins);
	
		// highlight the account cirle which is logged in
		var search = $(".btree-item-content").filter(function()
		{
			return $(this).text().toLowerCase().indexOf(user.accountId.toLowerCase()) >= 0;
		}).first();
	
		search.parent().addClass("account-login");
	
		// disable rest of the account circles
		var itemsToDisableClicking = $(".btree-item").filter(function()
		{
			return $(this).text().toLowerCase().indexOf(user.accountId.toLowerCase()) < 0;
		});
	
		itemsToDisableClicking.off('click');
		
		ad = getFromGM(enums.labels.ad);
		
		if(ad.automate)
		{
			//since this is automation process its better to show
			//account progress by changing color of completed ones
			var selfAutomate = getFromGM(enums.labels.automate);

			if(selfAutomate.completed != null)
			{
				var completedLogins = selfAutomate.completed.account;
				
				// show completed accounts progress
				//get accounts which are completed
				var itemsToHighlightCompleted = $(".btree-item").filter(function()
				{
					if($(this).first().text() <= completedLogins)
					{
						return $(this);
					}
				});
				
				//add highlight color to the accounts which are completed
				itemsToHighlightCompleted.each(function()
				{
					$(this).addClass("account-highlight");
				});
			}
		}
		else
		{
			//since its not automation process its sensible to show
			//account actions for user 
			search.click();
		}
		//{"login":null,"status":"Stopped","completed":null,"currentStep":null,"nextStep":null,"accountStatus":"Stopped","processingLogin":null}
	}

}


function processAdsMethod2()
{
	var viewAdButtonsList = $(uiElements.inputElements.adButtons);
	
	$.each(viewAdButtonsList, function(key, value)
	{
		var adCode = value.parentNode.parentNode.cells[1].textContent;

		var adProcess = {};
		adProcess.adCode = adCode;
		adProcess.value = value.nextSibling.nextSibling.value;
		adProcess.name = value.nextSibling.nextSibling.name;

		adProcessList.push(adProcess);
	});

	ad.totalAds = adProcessList.length;
	ad.width = 0;
	ad.processedAds = 0;
	saveToGM(enums.labels.ad, ad);
	
	// submit ads in background with predefined delay
	sendDelay();
}



function processAdsMethod1()
{
	ad = getFromGM(enums.labels.ad);
	if (ad.method == enums.ad.method.click)
	{
		if (ad.processing)
		{
			ad.processWidth = false;
			setupProgressBar(ad.width, ad.processedAds, ad.totalAds, ad.processWidth);

			timeoutId = window.setTimeout(clickDelay(), delay.page.min);
		}
	}
}



function clickDelay()
{
	var adObject =
	{
		adCode : "",
		releaseDate : "",
		endDate : "",
		adID : "",
		buttonObject : "",
		hiddenObject : ""
	},
	viewAdButtonsList = $.find(uiElements.inputElements.adButtons),
	adButtonsCount = viewAdButtonsList.length;

	// getting stored width variable because its not going to stay after
	// background function calls
	ad = getFromGM(enums.labels.ad);

	delay.background = getRandomBetweenMilliseconds(delay.automation.min, delay.automation.max);

	if (adButtonsCount > 0)
	{
		var adRow = viewAdButtonsList[0].parentNode.parentNode.cells;

		adObject.adCode = adRow[1].textContent;
		adObject.releaseDate = adRow[2].textContent;
		adObject.endDate = adRow[3].textContent;
		adObject.adID = adRow[4].childNodes[3].value;
		adObject.buttonObject = adRow[4].childNodes[1];
		adObject.hiddenObject = adRow[4].childNodes[3];

		clickLink(adObject.buttonObject);
		ad.processedAds++;

		// since here the real progress bar applies we process width to change
		ad.processWidth = true;
		saveToGM(enums.labels.ad, ad);
		// console.log(ad);

		setupProgressBar(ad.width, ad.totalAds, ad.processedAds, ad.processWidth);

		console.log("clicked " + adObject.adCode + " with delay : " + delay.background);
		// console.log(adObject);

		window.setTimeout(reload, delay.page.min);
	}
	else
	{
		ad.processing = false;
		saveToGM(enums.labels.ad, ad);
	}
}


function drawBinaryTreeLines()
{
	tree = getFromGM(enums.labels.tree);
	
	for (var i = 1; i <= tree.totalRows; i++)
	{
		for (var j = 1; j <= Object.keys(tree.hierarchy).length; j++)
		{
			for (var k = 0; k < 2; k++)
			{
				var connectionFrom = "",
					connectionTo = "",
					withinRow = "";

				connectionFrom = 'div.a' + j;
				connectionTo = 'div.a' + tree.hierarchy[j][k];
				withinRow = '.row' + i;

				//console.log(connectionFrom + " -> " + connectionTo + " within" + withinRow);

				// create connections to draw lines
				$(connectionFrom).connections(
				{
					to : connectionTo,
					within : withinRow
				});
			}
			if (k == tree.breakPoints[j])
				continue;
		}
	}
}

function automation()
{

	var selfAutomate = getFromGM(enums.labels.automate),
		totalLogins = Object.keys(logins).length,
		loginId = 1,
		loginData = null;
	
	//first time processing accounts
	//since nothing will be stored in gm we need to set data
	if(selfAutomate == null)
	{
		selfAutomate = automate;
	}
	
	//previously processed all accounts and now it needs to reset
	if(selfAutomate.completed != null)
	{
		if(selfAutomate.completed.account == totalLogins)
		{
			console.log("reset automate to default for another round");
			selfAutomate == automate;
		}
	}
	
	//automation is stopped by detault so we start with first account
	//then we move to next account based on its status
	switch (selfAutomate.status)
	{
		case enums.automation.status.stopped:
			
			//check if a username is already logged in previously then 
			//login next ones
			//completed will have previous login details so it must be null for 
			//first time
			if(selfAutomate.completed == null)
			{
				if(selfAutomate.accountStatus == enums.automation.status.stopped)
				{
					//first attempt so nothing is completed previously
					//go with first one
					loginData = logins[loginId];
					console.log("Processing .. " + loginData.username);
					
					selfAutomate.nextStep = enums.automation.steps.login;
					selfAutomate.accountStatus = enums.automation.status.active;
					selfAutomate.processingLogin = loginData;
					saveToGM(enums.labels.automate, selfAutomate);
				}
			}
			
			break;
		case enums.automation.status.active:
			
			if(selfAutomate.accountStatus == enums.automation.status.stopped)
			{
				loginId = selfAutomate.completed.account;
				
				//goto next login
				loginId++;
				
				loginData = logins[loginId];
				console.log("Processing .. " + loginData.username);
				
				selfAutomate.nextStep = enums.automation.steps.login;
				selfAutomate.accountStatus = enums.automation.status.active;
				selfAutomate.processingLogin = loginData;
				saveToGM(enums.labels.automate, selfAutomate);
			}
			
			break;
		case enums.automation.status.done:
			
			ad.automate = false;
			
			saveToGM(enums.labels.ad, ad);
			
			//reset to default object
			selfAutomate == automate;
			
			console.log("Automation Done");
			
			break;
	}
	
	//start processing only when account is being processed
	//process account only when its active
	if(selfAutomate.accountStatus == enums.automation.status.active)
	{
		switch (selfAutomate.nextStep)
		{
			case enums.automation.steps.login:
				automateTimeoutId = window.setTimeout(automateLogin, delay.background);
				break;
			
			case enums.automation.steps.securityCheck:
				//get the security password for this account and submit it 
				automateTimeoutId = window.setTimeout(automatedSecurityCheck, delay.background);
				break;
			
			case enums.automation.steps.processingAds:
				//process the ads automatically generally by xhttp method 
				automateTimeoutId = window.setTimeout(automatedProcessAds, delay.background);
				break;
			
			case enums.automation.steps.logout:
				automateTimeoutId = window.setTimeout(automateLogout, delay.background);
				break;
		}
	}
}



function automateLogin()
{
	var selfAutomate = getFromGM(enums.labels.automate);
	user = getFromGM(enums.labels.user);
	
	if(selfAutomate == null) 
	{
		selfAutomate = automate;
	}
	
	selfAutomate.status = enums.automation.status.active;
	selfAutomate.currentStep = enums.automation.steps.login;
	selfAutomate.nextStep = enums.automation.steps.securityCheck;
	saveToGM(enums.labels.automate, selfAutomate);
	
	user.userId = selfAutomate.processingLogin.username;
	user.accountId = selfAutomate.processingLogin.account;
	
	saveToGM(enums.labels.user, user);
	
	// set form inputs
	$(uiElements.inputElements.loginUsername).val(selfAutomate.processingLogin.username);
	$(uiElements.inputElements.loginPassword).val(selfAutomate.processingLogin.password);

	// find the form that this link is in and trigger a submit event for the
	// form
	//$("#mail_frm").submit();
	$(".btn-primary").first().click();
}

function automateLogout()
{
	var selfAutomate = getFromGM(enums.labels.automate);
	user = getFromGM(enums.labels.user);
	var totalLogins = Object.keys(logins).length;
	
	selfAutomate.currentStep = enums.automation.steps.logout;
	selfAutomate.nextStep = null;
	selfAutomate.accountStatus = enums.automation.status.stopped;
	
	user.userId = "";
	user.accountId = "";
	user.isLoggedIn = false;
	
	selfAutomate.completed = selfAutomate.processingLogin;
	
	console.log("Done .. " + selfAutomate.completed.account + " --> " + selfAutomate.completed.username);
	
	//check if this is last account to be processed
	//after this stop automation
	if(selfAutomate.completed.account == totalLogins)
	{
		selfAutomate.status = enums.automation.status.done;
	}
	
	saveToGM(enums.labels.user, user);
	saveToGM(enums.labels.automate, selfAutomate);
	loadUrl(urls.logout);
}



function automatedSecurityCheck()
{
	
	var selfAutomate = getFromGM(enums.labels.automate);
	
	selfAutomate.currentStep = enums.automation.steps.securityCheck;
	selfAutomate.nextStep = enums.automation.steps.processingAds;
	
	saveToGM(enums.labels.automate, selfAutomate);
	
	//big headache with ajax requests
	//also with form submit and xhttp requests
	//because the post is returning 302 object moved 
	//and that is handled by browser and not javascript
	//so back to basics
	//@TODO previously tried many methods but now sticking to 
	//browser redirect to work. we can try again with newer url
	//and it will be lot faster
	loadUrl(urls.ads);
	
}



function automatedProcessAds()
{
	
	var selfAutomate = getFromGM(enums.labels.automate);
	
	selfAutomate.currentStep = enums.automation.steps.processingAds;
	selfAutomate.nextStep = enums.automation.steps.logout;
	
	saveToGM(enums.labels.automate, selfAutomate);
	
	if($(uiElements.inputElements.adButtons).length)
	{
		//process ads using xhttp method
		//this can be changed in future when only click is available
		processAdsMethod2();
	}
	else
	{
		automateLogout();
	}

}


function backButtonClickEventHandler()
{
	$(uiElements.containers.btree + ", " + uiElements.containers.account).toggle();
	$(uiElements.containers.btree).show();
	$(uiElements.containers.progressBar).hide();
	$(uiElements.containers.backButton).hide();
	$(uiElements.labels.account).html("Accounts");
	$(uiElements.labels.accountId).html("");
	$(uiElements.labels.userId).html("");

	$(uiElements.containers.rightButton).show(); // top right buttons
	
	ad = getFromGM(enums.labels.ad);
	if(ad.method == enums.ad.method.xhttp)
	{
		$(uiElements.toggle.xhttp).show();
		$(uiElements.toggle.click).hide();
	}
	else if(ad.method == enums.ad.method.click)
	{
		$(uiElements.toggle.click).show();
		$(uiElements.toggle.xhttp).hide();
	}

	// user is not logged in so we need to show login button the next time
	// we need to reset user object because user might try to login to another
	// accounts
	if (!user.isLoggedIn)
	{
		user.userId = "";
		user.accountId = "";
	}
}

function balanceButtonClickEventHandler()
{
	// console.log(urls.cash);
	loadUrl(urls.cash);
}

function withdrawButtonClickEventHandler()
{
	// console.log(urls.withdraw);
	loadUrl(urls.withdraw);
}

function processButtonClickEventHandler()
{
	// if already on ads page dont load it otherwise load it and do security
	// check
	if (window.location != urls.ads)
	{
		// reset values because we are on different page and coming to ads
		// processing
		// below values are only used when processing and not when changing urls
		ad.processedAds = 0;
		ad.width = 0;
		ad.processing = false;

		saveToGM(enums.labels.ad, ad);

		// console.log(urls.ads);
		loadUrl(urls.ads);
		return;
	}

	// make ui progress bar into start state
	resetProgressBar();

	var viewAdButtonsList = $(uiElements.inputElements.adButtons);

	if (viewAdButtonsList !== undefined || viewAdButtonsList !== null) 
	{
		ad.totalAds = viewAdButtonsList.length;
		ad.processedAds = 0;
		ad.width = 0;
		ad.processing = true;

		saveToGM(enums.labels.ad, ad);
		
		if (ad.method == enums.ad.method.xhttp)
		{
			processAdsMethod2();
		}
		else if (ad.method == enums.ad.method.click)
		{
			// click ads in background with predefined delay
			window.setTimeout(clickDelay, delay.background);
		}
	}

}

function loginButtonClickEventHandler()
{
	// get the account number based on the click
	var accountId = $(".top-right-content-account").text();
	user.accountId = accountId; 
	user.userId = logins[accountId].username;
	
	saveToGM(enums.labels.user, user);
	
	// get the account user details to login
	var username = logins[user.accountId].username,
		password = logins[user.accountId].password;

	// set form inputs
	$(uiElements.inputElements.loginUsername).val(username);
	$(uiElements.inputElements.loginPassword).val(password);

	// find the form that this link is in and trigger a submit event for the
	// form
	//$("#mail_frm").submit();
	$(".btn-primary").first().click();
}


function logoutButtonClickEventHandler()
{
	user.userId = "";
	user.accountId = "";
	user.isLoggedIn = false;
	saveToGM(enums.labels.user, user);
	// console.log(urls.logout);
	loadUrl(urls.logout);
}



function cancelButtonClickEventHandler()
{
	ad.processing = false;
	saveToGM(enums.labels.ad, ad);
	clearTimeout(timeoutId);
	console.log("timeout cleared");

	// hide cancel button
	$(uiElements.buttons.cancel).hide();

	// show resume button
	$(uiElements.buttons.resume).show();
}

function resumeButtonClickEventHandler()
{
	ad.processing = true;
	saveToGM(enums.labels.ad, ad);
	timeoutId = window.setTimeout(clickDelay, delay.background);
	console.log("timeout resumed");

	// hide resume button
	$(uiElements.buttons.resume).hide();

	// show cancel button
	$(uiElements.buttons.cancel).show();
}

function methodButtonClickEventHandler()
{
	var click = $(uiElements.toggle.click),
		xhttp = $(uiElements.toggle.xhttp);

	ad = getFromGM(enums.labels.ad);
	
	if ($(click).css('display') == 'none')
	{
		// element is hidden
		click.show();
		xhttp.hide();

		ad.method = enums.ad.method.click;
		saveToGM(enums.labels.ad, ad);
	}
	else
	{
		click.hide();
		xhttp.show();

		ad.method = enums.ad.method.xhttp;
		saveToGM(enums.labels.ad, ad);
	}
}

function automateButtonClickEventHandler()
{
	var automateOn = $(uiElements.toggle.automateon),
		automateOff = $(uiElements.toggle.automateoff);

	ad = (getFromGM(enums.labels.ad) == null) ? ad : getFromGM(enums.labels.ad);

	if ($(automateOn).css('display') == 'none')
	{
		// element is hidden
		automateOn.show();
		automateOff.hide();

		ad.automate = true;
		saveToGM(enums.labels.ad, ad);
		
		//checking if we have previous automation done once and reset it
		var selfAutomate = getFromGM(enums.labels.automate);
		if(selfAutomate != null)
		{
			if(selfAutomate.status == enums.automation.status.done)
			{
				console.log("previous session of automation object exists so resetting");
				saveToGM(enums.labels.automate, automate);
			}
		}
		
		automation();
	}
	else
	{
		automateOn.hide();
		automateOff.show();

		ad.automate = false;
		clearTimeout(automateTimeoutId);
		saveToGM(enums.labels.ad, ad);
	}
}

function btreeItemClickEventHandler(event)
{
	var self = event.target;

	// check whether user is logged in
	if (!isEmpty(user.userId))
	{
		// remove login-button from the account options because its already
		// logged in
		$(uiElements.buttons.login).hide();
	}
	else
	{
		// if not logged in then we can access any account and try to login into
		// it

		// remove logout-button from the account options because its already
		// logged in
		$(uiElements.buttons.logout).hide();
		$(uiElements.buttons.balance).hide();
		$(uiElements.buttons.withdraw).hide();
		$(uiElements.buttons.process).hide();
	}

	
	user.accountId = $(self).parent().text().trim();

	// get login details based on account clicked
	login.username = logins[user.accountId].username;
	user.userId = logins[user.accountId].username;

	// setting top row with account id and account code after account clicking
	$(uiElements.labels.account).html("");
	$(uiElements.labels.accountId).html(user.accountId);
	$(uiElements.labels.userId).html(user.userId);

	$(uiElements.containers.account).show(); // account options
	$(uiElements.containers.btree).hide(); // accounts tree
	$(uiElements.containers.backButton).show(); // back button

	$(uiElements.containers.rightButton).hide(); // top right buttons

}

// variables
var	username,
	password,
	securityPassword,
	userId,
	accountId,
	timeoutId,
	automateTimeoutId,
	walletBalance,
	delay={},
	elementsInRows=[],
	elementsTillRow=[],
	adProcessList = [],
	urls = {},
	tree = null,
	waitElements = null,
	logins = null,
	automatedStepsEnum = null,
	enums = null,
	uiElements = null,

	//objects
	money =
	{
		totalCredit : 0,
		totalDebit : 0,
		balance : 0
	},
	login =
	{
		username : "",
		password : "",
		security : ""
	},
	user =
	{
		userId : "",
		accountId : "",
		isLoggedIn : false
	},
	ad =
	{
		totalAds : 0,
		clickedAds : 0,
		processedAds : 0,
		tries : 0,
		width : 0,
		processing : false,
		processWidth : false,
		method : "xhttp", // or xhttp, click
		automate : false
	},
	automate =
	{
		login : null,
		status : "stopped", //Active or Stopped or Done (processing all logins done)
		accountStatus : "stopped",
		completed : null,
		processingLogin : null,
		currentStep : null, //login processads logout etc
		nextStep : null, //login processads logout etc
	};


//get data from config and load the objects with relevant data
getConfigData();

//gets data from gm or else set it to default objects
setupDataObjects();

//get custom html and style from web or disk
attachHTML();

// custom html operations
setupBtree();
drawBinaryTreeLines();

//setup ui for the first time. Handles all the hiding and showing ui elements
uiFirstRun();

//all button click event handlers
eventHandlers();

//reset session information on login or index pages
//set user information in dashboard etc
urlBasedActions();

// get userId which is already stored as session information during login
user = getFromGM(enums.labels.user);

//highlight account and disable clicking rest after login
postLoginAccountOperations();

//run automation if its enabled
if(ad.automate)
{
	automation();
}

//search for these elements and do necessary options
//waitForKeyElements(waitElements.balance, getWalletBalance);

//@TODO check with ad click method and see if its working
waitForKeyElements(waitElements.ads, processAdsMethod1);
