/**
 * 
 */

var data = 
{
	urls: 
    {
        cash : { url :  "login/cash_wallet.php", base : true },
		withdraw : { url :  "login/with_pay.php", base : true },
		ads : { url :  "login/my_ads.php", base : true },
		security : { url :  "login/security_check.php", base : true },
		login : { url :  "login.php", base : true },
		logout : { url :  "login/logout.php", base : true },
		index : { url :  "index.php", base : true },
		dashboard : { url :  "login/ibo_login.php", base : true },
		dashboardDesktop : { url :  "login/ibo_login.php?ic=D", base : true },
		ad : { url :  "get_ad_amt.php?cd=", base : false },
    },
    waitElements :
    {
    	login : "#mail_frm",
		userId : ".panel-body.list-group",
		security : "#txt_tran_pwd",
		balance : "font:contains(Cash Wallet Transactions)",
		ads : "font:contains(Daily Ads Log)",
    },
    uiElements :
    {
    	buttons : 
    	{
    		back : ".back-button",
    		balance : ".balance-button",
    		withdraw : ".withdraw-button",
    		process : ".process-button",
    		login : ".login-button",
    		logout : ".logout-button",
    		cancel : ".cancel-button",
    		resume : ".resume-button",
    		method : ".method-button",
    		automate : ".automate-button",
    	},
    	toggle :
    	{
    		click : ".click",
    		xhttp : ".xhttp",
    		automateon : ".automate-on",
    		automateoff : ".automate-off",
    	},
    	inputElements :
    	{
    		securityPassword : "#txt_tran_pwd",
    		securityCheckButton : ':input[value="Check"]',
    		adButtons : ':input[value="VIEW AD"]',
    		loginUsername : 'input[name="txt_un"]',
    		loginPassword : 'input[name="txt_pwd"]',
    	},
    	containers :
    	{
    		progressBar : "#progress-bar-container",
    		progressButtons : ".progress-buttons-container",
    		progress : ".progressbar",
    		account : "#account-container",
    		backButton : ".top-left-container",
    		rightButton : ".top-right-content-buttons",
    		admagic : "#admagicCustomHtml",
    		btree : "#btree-container",
    	},
    	labels :
    	{
    		account : ".accounts-label",
    		userId : ".top-right-content-code",
    		accountId : ".top-right-content-account",
    	}
    },
    tree :
    {
    	totalRows : null,
		greenBorder : "1px solid #34a853",
		blueBorder : "1px solid #4285F4",
		hierarchy : 
		{
			1 : [ 2, 3 ],
			2 : [ 4, 5 ],
			3 : [ 6, 7 ],
			4 : [ 8, 9 ],
			5 : [ 10, 11 ],
			6 : [ 12, 13 ],
			7 : [ 14, 15 ],
			8 : [ 16, 17 ],
			9 : [ 18, 19 ],
			10 : [ 20, 21 ],
			11 : [ 22, 23 ],
			12 : [ 24, 25 ],
			13 : [ 26, 27 ],
			14 : [ 28, 29 ],
			15 : [ 30, 31 ]
		},
		breakPoints : [ 1, 3, 7, 15 ]
    },
    enums :
    {
    	labels : 
    	{
    		automate : "automate",
    		ad : "ad",
    		user : "user",
    		login : "login",
    		tree : "tree"
    	},
    	automation :
    	{
    		steps : 
    		{
    			login : "login", 
				adPage : "adPage", 
				securityCheck : "securityCheck", 
				processingAds : "processingAds", 
				logout : "logout"
    		},
    		status :
    		{
    			active : "active",
    			done : "done",
    			stopped : "stopped"
    		}
    	},
    	ad : 
    	{
    		method :
    		{
    			click : "click",
    			xhttp : "xhttp"
    		}
    	}
    },
    delay :
    {
    	automation : 
    		{
    			min : 900,
    			max : 1500
    		},
    	page :
    		{
    			min : 1000
    		},
    	background : null
    },
    logins :
    {
    	1 :
		{
			username : "ADM5991758",
			password : "pass123",
			securityPassword : "pass123",
			parent : 0,
			status : "Active",
			isChild : false,
			row : 1,
			account : 1
		},
		2 :
		{
			username : "ADM5489878",
			password : "pass123",
			securityPassword : "pass123",
			parent : 1,
			status : "Active",
			isChild : false,
			row : 2,
			account : 2
		},
		3 :
		{
			username : "ADM6215472",
			password : "pass123",
			securityPassword : "pass123",
			parent : 1,
			status : "Active",
			isChild : false,
			row : 2,
			account : 3
		},
		4 :
		{
			username : "ADM4154232",
			password : "pass123",
			securityPassword : "pass123",
			parent : 2,
			status : "Active",
			isChild : false,
			row : 3,
			account : 4
		},
		5 :
		{
			username : "ADM7429751",
			password : "pass123",
			securityPassword : "pass123",
			parent : 2,
			status : "Active",
			isChild : false,
			row : 3,
			account : 5
		},
		6 :
		{
			username : "ADM8634728",
			password : "pass123",
			securityPassword : "pass123",
			parent : 3,
			status : "Active",
			isChild : false,
			row : 3,
			account : 6
		},
		7 :
		{
			username : "ADM1492591",
			password : "pass123",
			securityPassword : "pass123",
			parent : 3,
			status : "Active",
			isChild : false,
			row : 3,
			account : 7
		},
		8 :
		{
			username : "ADM6112881",
			password : "pass123",
			securityPassword : "pass123",
			parent : 4,
			status : "Active",
			isChild : false,
			row : 4,
			account : 8
		},
		9 :
		{
			username : "ADM1925568",
			password : "pass123",
			securityPassword : "pass123",
			parent : 4,
			status : "Active",
			isChild : false,
			row : 4,
			account : 9
		},
		10 :
		{
			username : "ADM7118922",
			password : "pass123",
			securityPassword : "pass123",
			parent : 5,
			status : "Active",
			isChild : false,
			row : 4,
			account : 10
		},
		11 :
		{
			username : "ADM3449437",
			password : "pass123",
			securityPassword : "pass123",
			parent : 5,
			status : "Active",
			isChild : false,
			row : 4,
			account : 11
		},
		12 :
		{
			username : "ADM3793443",
			password : "pass123",
			securityPassword : "pass123",
			parent : 6,
			status : "Active",
			isChild : false,
			row : 4,
			account : 12
		},
		13 :
		{
			username : "ADM9522143",
			password : "pass123",
			securityPassword : "pass123",
			parent : 6,
			status : "Active",
			isChild : false,
			row : 4,
			account : 13
		},
		14 :
		{
			username : "ADM6927984",
			password : "pass123",
			securityPassword : "pass123",
			parent : 7,
			status : "Active",
			isChild : false,
			row : 4,
			account : 14
		},
		15 :
		{
			username : "ADM3486915",
			password : "pass123",
			securityPassword : "pass123",
			parent : 7,
			status : "Active",
			isChild : false,
			row : 4,
			account : 15
		},
	
		16 :
		{
			username : "ADM1716158",
			password : "pass123",
			securityPassword : "pass123",
			parent : 8,
			status : "Active",
			isChild : true,
			row : 5,
			account : 16
		},
		17 :
		{
			username : "ADM8164742",
			password : "pass123",
			securityPassword : "pass123",
			parent : 8,
			status : "Active",
			isChild : true,
			row : 5,
			account : 17
		},
		18 :
		{
			username : "ADM5721562",
			password : "pass123",
			securityPassword : "pass123",
			parent : 9,
			status : "Active",
			isChild : true,
			row : 5,
			account : 18
		},
		19 :
		{
			username : "ADM6135933",
			password : "pass123",
			securityPassword : "pass123",
			parent : 9,
			status : "Active",
			isChild : true,
			row : 5,
			account : 19
		},
		20 :
		{
			username : "ADM3241864",
			password : "pass123",
			securityPassword : "pass123",
			parent : 10,
			status : "Active",
			isChild : true,
			row : 5,
			account : 20
		},
		21 :
		{
			username : "ADM5471355",
			password : "pass123",
			securityPassword : "pass123",
			parent : 10,
			status : "Active",
			isChild : true,
			row : 5,
			account : 21
		},
		22 :
		{
			username : "ADM3897812",
			password : "pass123",
			securityPassword : "pass123",
			parent : 11,
			status : "Active",
			isChild : true,
			row : 5,
			account : 22
		},
		23 :
		{
			username : "ADM2173114",
			password : "pass123",
			securityPassword : "pass123",
			parent : 11,
			status : "Active",
			isChild : true,
			row : 5,
			account : 23
		},
		24 :
		{
			username : "ADM5475648",
			password : "pass123",
			securityPassword : "pass123",
			parent : 12,
			status : "Active",
			isChild : true,
			row : 5,
			account : 24
		},
		25 :
		{
			username : "ADM2111612",
			password : "pass123",
			securityPassword : "pass123",
			parent : 12,
			status : "Active",
			isChild : true,
			row : 5,
			account : 25
		},
		26 :
		{
			username : "ADM9229184",
			password : "pass123",
			securityPassword : "pass123",
			parent : 13,
			status : "Active",
			isChild : true,
			row : 5,
			account : 26
		},
		27 :
		{
			username : "ADM9649193",
			password : "pass123",
			securityPassword : "pass123",
			parent : 13,
			status : "Active",
			isChild : true,
			row : 5,
			account : 27
		},
		28 :
		{
			username : "ADM6166652",
			password : "pass123",
			securityPassword : "pass123",
			parent : 14,
			status : "Active",
			isChild : true,
			row : 5,
			account : 28
		},
		29 : 
		{
			username : "ADM2614953",
			password : "pass123",
			securityPassword : "pass123",
			parent : 14,
			status : "Active",
			isChild : true,
			row : 5,
			account : 29
		},
		30 : 
		{
			username : "ADM7484547",
			password : "pass123",
			securityPassword : "pass123",
			parent : 15,
			status : "Active",
			isChild : true,
			row : 5,
			account : 30
		},
		31 : 
		{
			username : "ADM9712315",
			password : "pass123",
			securityPassword : "pass123",
			parent : 15,
			status : "Active",
			isChild : true,
			row : 5,
			account : 31
		}
    },
};