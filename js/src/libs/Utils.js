function loadUrl(newLocation)
{
	window.location = newLocation;
	return false;
}

function getBaseURL()
{
	return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}

function reload()
{
	location.reload();
}

function getRandomBetweenMilliseconds(lower, upper)
{
	// var x = Math.random() * (2000-3000+1)+3000);
	var milliseconds = Math.floor(Math.random() * (lower - upper + 1) + upper);
	milliseconds = Math.round(milliseconds / 100) * 100;
	return milliseconds;
}

function isEmpty(str)
{
	return (!str || 0 === str.length);
}

function handle_mousedown(e)
{
	window.my_dragging = {};
	my_dragging.pageX0 = e.pageX;
	my_dragging.pageY0 = e.pageY;
	my_dragging.elem = this;
	my_dragging.offset0 = $(this).offset();
	function handle_dragging(e)
	{
		var left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0),
			top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
		$(my_dragging.elem).offset(
		{
			top : top,
			left : left
		});
	}
	function handle_mouseup(e)
	{
		$('body').off('mousemove', handle_dragging).off('mouseup', handle_mouseup);
	}
	$('body').on('mouseup', handle_mouseup).on('mousemove', handle_dragging);
}

function saveToGM(name, obj)
{
	/*
	 * for(var i in obj) GM_setValue(i+'_store',obj[i]);
	 */
	GM_setValue(name, JSON.stringify(obj));
}

function getFromGM(name)
{
	// obj=eval(GM_getValue(name));
	return JSON.parse(GM_getValue(name));
}